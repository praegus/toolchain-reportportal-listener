package nl.praegus.fitnesse.junit;

import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

import static java.lang.String.format;
import static org.mockserver.model.HttpRequest.request;

public class Mockserver {
    private final int port = 1337;
    private ClientAndServer mockserver;

    private HttpRequest startLaunch = request().withMethod("POST").withPath("/api/v1/default_personal/launch/");
    private HttpRequest startSuite = request().withMethod("POST").withPath("/api/v1/default_personal/item");
    private HttpRequest startTest;
    private HttpRequest finishTest;
    private HttpRequest logRequest = request().withMethod("POST").withPath("/api/v1/default_personal/log");

    public Mockserver() {
        this.mockserver = ClientAndServer.startClientAndServer(port);
    }

    public void mockStartLaunch(String id) {

        mockserver
                .when(startLaunch)
                .respond(HttpResponse
                        .response()
                        .withStatusCode(200)
                        .withHeader(new Header("Content-Type", "application/json"))
                        .withBody(format("{\"id\":\"%s\"}", id)));
    }

    public HttpRequest[] getStartLaunchRequests() {
        return mockserver.retrieveRecordedRequests(startLaunch);
    }

    public void mockLog() {
        mockserver
                .when(logRequest)
                .respond(HttpResponse
                        .response()
                        .withHeader(new Header("Content-Type", "application/json"))
                        .withStatusCode(200));
    }

    public HttpRequest[] getLogRequest() {
        return mockserver.retrieveRecordedRequests(logRequest);
    }

    public void mockStartSuite(String id) {
        mockserver
                .when(startSuite)
                .respond(HttpResponse
                        .response()
                        .withStatusCode(200)
                        .withHeader(new Header("Content-Type", "application/json"))
                        .withBody(format("{\"id\":\"%s\"}", id)));
    }

    public HttpRequest[] getStartSuiteRequests() {
        return mockserver.retrieveRecordedRequests(startSuite);
    }

    public void stop() {
        mockserver.stop();
    }

    public void mockStartTest(String suiteId, String testId) {
        this.startTest = request().withMethod("POST").withPath(format("/api/v1/default_personal/item/%s", suiteId));
        mockserver
                .when(startTest)
                .respond(HttpResponse
                        .response()
                        .withStatusCode(200)
                        .withHeader(new Header("Content-Type", "application/json"))
                        .withBody(format("{\"id\":\"%s\"}", testId)));
    }

    public HttpRequest[] getStartTestRequest() {
        return mockserver.retrieveRecordedRequests(startTest);
    }

    public void mockFinishTest(String testId) {
        this.finishTest = request().withMethod("PUT").withPath(format("/api/v1/default_personal/item/%s", testId));
        mockserver
                .when(finishTest)
                .respond(HttpResponse
                        .response()
                        .withStatusCode(200)
                        .withHeader(new Header("Content-Type", "application/json"))
                        .withBody("{\"msg\":\"PASSED\"}"));
    }

    public void mockFinishSuite(String suiteId) {
        mockserver
                .when(request().withMethod("PUT").withPath(format("/api/v1/default_personal/item/%s", suiteId)))
                .respond(HttpResponse
                        .response()
                        .withStatusCode(200)
                        .withHeader(new Header("Content-Type", "application/json"))
                        .withBody("{\"msg\":\"PASSED\"}"));
    }

    public HttpRequest[] getFinishTestRequest() {
        return mockserver.retrieveRecordedRequests(finishTest);
    }

    public int getNumberOfRequests() {
        HttpRequest[] requests = mockserver.retrieveRecordedRequests(new HttpRequest());
        return requests.length;
    }

    public void mockFinishLaunch(String launchId) {
        mockserver
                .when(request().withMethod("PUT").withPath(format("/api/v1/default_personal/launch/%s/finish", launchId)))
                .respond(HttpResponse
                        .response()
                        .withStatusCode(200)
                        .withHeader(new Header("Content-Type", "application/json"))
                        .withBody("{\"msg\":\"PASSED\"}"));

    }
}
