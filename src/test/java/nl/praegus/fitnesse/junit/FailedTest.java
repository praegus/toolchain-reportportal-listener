package nl.praegus.fitnesse.junit;

import com.epam.ta.reportportal.ws.model.FinishTestItemRQ;
import com.epam.ta.reportportal.ws.model.StartTestItemRQ;
import com.epam.ta.reportportal.ws.model.launch.StartLaunchRQ;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.praegus.fitnesse.junit.dummy.TestFailed;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.mockserver.model.HttpRequest;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@Ignore // kan aangezet worden om te kijken of er goed mee omgegaan wordt; test zal logischerwijs falen
@RunWith(Suite.class)
@Suite.SuiteClasses({TestFailed.class})
public class FailedTest {
    private static Mockserver mockserver;
    private static ObjectMapper mapper = new ObjectMapper();

    @BeforeClass
    public static void startMockserver() {
        String launchId = "launch";
        String suiteId = "suiteId";
        String testId = "testId";

        mockserver = new Mockserver();
        mockserver.mockStartLaunch(launchId);
        mockserver.mockStartSuite(suiteId);
        mockserver.mockStartTest(suiteId, testId);
        mockserver.mockLog();
        mockserver.mockFinishTest(testId);
        mockserver.mockFinishSuite(suiteId);
        mockserver.mockFinishLaunch(launchId);
    }

    @AfterClass
    public static void after() throws IOException {
        checkStartLaunchRequest();
        checkStartSuiteRequest();
        checkStartTestRequest();
        checkLogRequest();
        checkFinishTestRequest();

        assertThat(mockserver.getNumberOfRequests()).isEqualTo(5);
    }

    private static void checkStartLaunchRequest() throws IOException {
        HttpRequest[] requests = mockserver.getStartLaunchRequests();
        assertThat(requests).hasSize(1);
        StartLaunchRQ request = mapper.readValue(requests[0].getBodyAsString(), StartLaunchRQ.class);

        assertThat(request.getMode().name()).isEqualTo("DEFAULT");
        assertThat(request.getName()).isEqualTo("default_JUNIT_AGENT");
    }

    private static void checkStartSuiteRequest() throws IOException {
        HttpRequest[] requests = mockserver.getStartSuiteRequests();
        assertThat(requests).hasSize(1);
        StartTestItemRQ request = mapper.readValue(requests[0].getBodyAsString(), StartTestItemRQ.class);

        assertThat(request.getLaunchId()).isEqualTo("launch");
        assertThat(request.getType()).isEqualTo("SUITE");
        assertThat(request.getName()).isEqualTo("default");
        assertThat(request.getStartTime()).isNotNull();
    }

    private static void checkStartTestRequest() throws IOException {
        HttpRequest[] requests = mockserver.getStartTestRequest();
        assertThat(requests).hasSize(1);
        StartTestItemRQ request = mapper.readValue(requests[0].getBodyAsString(), StartTestItemRQ.class);
        assertThat(request.getLaunchId()).isEqualTo("launch");
        assertThat(request.getType()).isEqualTo("TEST");
        assertThat(request.getName()).isEqualTo("TestFailed");
        assertThat(request.getStartTime()).isNotNull();
    }

    private static void checkLogRequest() {
        HttpRequest[] requests = mockserver.getLogRequest();
        assertThat(requests).hasSize(1);
        assertThat(requests[0].getBodyAsString()).contains("Content-Disposition: form-data; name=\"binary_part\"; filename=\"Page Screenshot\"");
        assertThat(requests[0].getBodyAsString()).contains("Content-Type: application/octet-stream");
        assertThat(requests[0].getBodyAsString()).contains("Content-Transfer-Encoding: binary");
    }

    private static void checkFinishTestRequest() throws IOException {
        HttpRequest[] requests = mockserver.getFinishTestRequest();
        assertThat(requests).hasSize(1);
        FinishTestItemRQ request = mapper.readValue(requests[0].getBodyAsString(), FinishTestItemRQ.class);
        assertThat(request.getStatus()).isEqualTo("FAILED");
        assertThat(request.getEndTime()).isNotNull();
    }
}

