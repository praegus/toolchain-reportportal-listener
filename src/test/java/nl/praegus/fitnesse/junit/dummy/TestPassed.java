package nl.praegus.fitnesse.junit.dummy;

import fitnesse.junit.FitNesseRunner;
import nl.praegus.fitnesse.junit.runners.ReportPortalEnabledToolchainTestRunner;
import org.junit.runner.RunWith;

/**
 * Test class to allow fixture code to be debugged, or run by build server.
 */
@RunWith(ReportPortalEnabledToolchainTestRunner.class)
@FitNesseRunner.Suite("TestPassed")
public class TestPassed {
}
