package nl.praegus.fitnesse.junit;

import com.epam.ta.reportportal.ws.model.FinishTestItemRQ;
import com.epam.ta.reportportal.ws.model.StartTestItemRQ;
import com.epam.ta.reportportal.ws.model.launch.StartLaunchRQ;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.praegus.fitnesse.junit.dummy.TestPassed;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.mockserver.model.HttpRequest;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestPassed.class})
public class PassedTest {
    private static Mockserver mockserver;
    private static ObjectMapper mapper = new ObjectMapper();

    @BeforeClass
    public static void startMockserver() {
        String launchId = "launch";
        String suiteId = "suiteId";
        String testId = "testId";

        mockserver = new Mockserver();
        mockserver.mockStartLaunch(launchId);
        mockserver.mockStartSuite(suiteId);
        mockserver.mockStartTest(suiteId, testId);
        mockserver.mockLog();
        mockserver.mockFinishTest(testId);
        mockserver.mockFinishSuite(suiteId);
        mockserver.mockFinishLaunch(launchId);
    }

    @AfterClass
    public static void after() throws IOException {
        checkStartLaunchRequest();
        checkStartSuiteRequest();
        checkStartTestRequest();
        checkLogRequest();
        checkFinishTestRequest();

        assertThat(mockserver.getNumberOfRequests()).isEqualTo(5);
    }

    private static void checkStartLaunchRequest() throws IOException {
        HttpRequest[] requests = mockserver.getStartLaunchRequests();
        assertThat(requests).hasSize(1);
        StartLaunchRQ request = mapper.readValue(requests[0].getBodyAsString(), StartLaunchRQ.class);

        assertThat(request.getMode().name()).isEqualTo("DEFAULT");
        assertThat(request.getName()).isEqualTo("default_JUNIT_AGENT");
    }

    private static void checkStartSuiteRequest() throws IOException {
        HttpRequest[] requests = mockserver.getStartSuiteRequests();
        assertThat(requests).hasSize(1);
        StartTestItemRQ request = mapper.readValue(requests[0].getBodyAsString(), StartTestItemRQ.class);

        assertThat(request.getLaunchId()).isEqualTo("launch");
        assertThat(request.getType()).isEqualTo("SUITE");
        assertThat(request.getName()).isEqualTo("default");
        assertThat(request.getStartTime()).isNotNull();
    }

    private static void checkStartTestRequest() throws IOException {
        HttpRequest[] requests = mockserver.getStartTestRequest();
        assertThat(requests).hasSize(1);
        StartTestItemRQ request = mapper.readValue(requests[0].getBodyAsString(), StartTestItemRQ.class);
        assertThat(request.getLaunchId()).isEqualTo("launch");
        assertThat(request.getType()).isEqualTo("TEST");
        assertThat(request.getName()).isEqualTo("TestPassed");
        assertThat(request.getStartTime()).isNotNull();
    }

    private static void checkLogRequest() {
        HttpRequest[] requests = mockserver.getLogRequest();
        assertThat(requests).hasSize(1);
        assertThat(requests[0].getBodyAsString()).contains("filename=\"Wiki Content\"");
        assertThat(requests[0].getBodyAsString()).contains("!|Import");
        assertThat(requests[0].getBodyAsString()).contains("|fitnesse.fixtures");
        assertThat(requests[0].getBodyAsString()).contains("|script|echo fixture");
        assertThat(requests[0].getBodyAsString()).contains("|check |echo|Hello|Hello|");
    }

    private static void checkFinishTestRequest() throws IOException {
        HttpRequest[] requests = mockserver.getFinishTestRequest();
        assertThat(requests).hasSize(1);
        FinishTestItemRQ request = mapper.readValue(requests[0].getBodyAsString(), FinishTestItemRQ.class);
        assertThat(request.getStatus()).isEqualTo("PASSED");
        assertThat(request.getEndTime()).isNotNull();
    }
}

