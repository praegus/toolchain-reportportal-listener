package nl.praegus.fitnesse;

import fitnesse.testsystems.*;
import nl.praegus.fitnesse.testsystemlisteners.util.PlainHtmlChunkParser;

import java.io.Closeable;
import java.io.IOException;

/*
 TestSystemListener that keeps a static StringBuilder for the running testcase
 The sb contains all the concatenated output chunks that are returned from the test system.
 The html contained in the sb is standalone. No external resources are required. CSS, JS and
 images (such as screen shots) are embedded in the page.

 To obtain the html, simply call StandaloneHtmlListener.output from your testrunlistener.
 Note: this cass is not usable when doing parallel execution from the same workspace.
 */

public class StandaloneHtmlListener implements TestSystemListener, Closeable {
    public static StringBuilder output = new StringBuilder();
    private PlainHtmlChunkParser parser = new PlainHtmlChunkParser();

    @Override
    public void testSystemStarted(TestSystem testSystem) {

    }

    @Override
    public void testOutputChunk(String chunk) {
        if(!chunk.isEmpty()) {
            chunk = parser.embedImages(chunk);
        }
        output.append(chunk);
    }

    @Override
    public void testStarted(TestPage testPage) {
        output = new StringBuilder();
        parser.initializeStandalonePage(testPage, output);
    }


    @Override
    public void testComplete(TestPage testPage, TestSummary testSummary) {
        parser.finalizeStandalonePage(output);
    }

    @Override
    public void testSystemStopped(TestSystem testSystem, Throwable cause) {

    }

    @Override
    public void testAssertionVerified(Assertion assertion, TestResult testResult) {

    }

    @Override
    public void testExceptionOccurred(Assertion assertion, ExceptionResult exceptionResult) {

    }

    @Override
    public void close() throws IOException {

    }
}
